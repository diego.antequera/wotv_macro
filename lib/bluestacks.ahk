#SingleInstance, Force
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir% ; Ensures a consistent starting directory.
DetectHiddenWindows, On

ANDROID_TITLE = BlueStacks
GAME_CMD = "C:\Program Files\BlueStacks\HD-RunApp.exe" -json "{\"app_icon_url\": \"\"`, \"app_name\": \"WOTV FFBE\"`, \"app_url\": \"\"`, \"app_pkg\": \"com.square_enix.android_amazon.WOTVffbeww\"}"
ANDROID_WIDTH := 900 ;fixed width of android emulator. If you change this, the image recognition files may not work properly
global IMAGE_SEARCH_ENABLE := true

global ANDROID_ID
global EMULATOR_LAUNCH_WAIT := 180000 ;time (in ms) to wait for emulator to finish starting up
global CHALLENGE_WAIT := 300000 ;time (in ms) to wait after starting an auto-battle on a challenge - i.e. 300000ms = 5 minutes
global BATTLE_WAIT := 120000 ; time (in ms) to wait after starting an auto-battle
global LAST_SLEEP_PREVENT := -1
global PREVENT_SLEEP_INTERVAL := 60000

global GAME_EXE := GAME_CMD
global GAME_TITLE := ANDROID_TITLE
global runInBackground := true
global displayController := !runInBackground
global preventSleep := runInBackground
; OnExit, EXIT_LABEL

activateEmulator() {
  if (!WinExist(GAME_TITLE))
  {
    ANDROID_ID := WinExist(GAME_TITLE)
    Run %ComSpec% /c "%GAME_EXE%
    notify("Launching Android emulator...")
    WinWait %GAME_TITLE%
    sleep 4000
    WinGet ANDROID_ID, ID, %GAME_TITLE%
    debug("Emulator window found: " . ANDROID_ID)
    resizeWindow()
    if (!waitForImageAndClick("gameStart", 100, 60000)) {
      killEmulator()
      Reload
    }
  }
  if (ANDROID_ID != WinExist(GAME_TITLE))
    ANDROID_ID := WinExist(GAME_TITLE)

  if (!runInBackground) 
    resizeWindow()

  if(preventSleep)
    preventFromSleeping()

}

isTheGameRunning() {
  return findImage("gameTitle", 0, 0, 0, 0, false, false)
}

preventFromSleeping() {
  if (LAST_SLEEP_PREVENT == -1) {
    LAST_SLEEP_PREVENT := A_TickCount
  }
  else if (A_TickCount > LAST_SLEEP_PREVENT + PREVENT_SLEEP_INTERVAL) {
    LAST_SLEEP_PREVENT := A_TickCount
    MouseMove, 1, 0, 1, R ;Move the mouse one pixel to the right
    MouseMove, -1, 0, 1, R ;Move the mouse back one pixel
  }
  return
}

resizeWindow() {
  if (!runInBackground) {
    WinActivate ahk_id %ANDROID_ID%
    WinMove, ahk_id %ANDROID_ID%,,0,0, 1039, 592
  }
}
killEmulator() {
  process, close, BlueStacks.exe
}

getWindowName() {
  return GAME_TITLE
}

getWindowId() {
  return ANDROID_ID
}