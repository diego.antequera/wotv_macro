#includeAgain lib/imageSize.ahk
SetBatchLines -1 ;-- To start

global imgHaystackFile := "lib/haystack.png"
global imgCache := []

findFirstImageAndClick(fileNames, clickDelay := 50, delay := 300000, cooldown := 500) {
  sleep delay
  for i, fileName in fileNames {
    debug("[findFirstImageAndClick] searching button " . fileName)
    sleep clickDelay
    if (clickOnImage(fileName, cooldown)) {
      return true
    }
    else {
      continue
    }
  }
  return false
}

waitForImageAndClick(fileName, clickDelay := 500, delay := 300000, cooldown := 500, sleepTime := 500){
  ; debug("[waitForImageAndClick] searching for img named " . fileName)
  img := waitForImage(fileName, delay, sleepTime)
  if (img != false) {
    ; debug("[waitForImageAndClick] Found! Clicking now!")
    sleep clickDelay
    return clickOnImageCoords(img, cooldown)
  }
  else {
    ; error("[waitForImageAndClick] failed to click; waitForImage failed failed for img " . fileName)
    return false
  }
}

clickOnImage(fileName, cooldown := 100, xMultiplier := 0.5, yMultiplier := 0.5, useCache := false, activateEmulator := true) {
  img := findImage(fileName, imgX, imgY, imgWidth, imgHeight, useCache, activateEmulator)
  if (img != false) {
    return clickOnImageCoords(img, cooldown, xMultiplier, yMultiplier)
  }
  else {
    ; error("[clickOnImage] failed to click; waitForImage failed failed for img " . fileName)
    return false
  }
}

waitForImage(fileName, delay := 300000, sleepTime := 500, msgComplete := "Found image", msgTimeout := "Image find timed out, continuing...") {
  _starttime := A_TickCount 
  loop {
    img := findImage(fileName, imgX, imgY, imgWidth, imgHeight)
    elapsedtime := A_TickCount - _starttime
    ; debug("[waitForImageAndClick] elapsedtime: " . elapsedtime)
    if (img != false) {
      ; debug("[waitForImageAndClick] " . msgComplete)
      return img
      break
    } else if (elapsedtime > delay) {
      ; error("[waitForImageAndClick] " . msgTimeout)
      return false
      break
    }
  }
  sleep sleepTime
}

clickOnImageCoords(img, cooldown := 100, xMultiplier := 0.5, yMultiplier := 0.5){
  if (img != false) {
    if (runInBackground) {
      imgX := % "X" . Floor((img.x + (img.width * xMultiplier)))
      imgY := % "Y" . Floor((img.y + (img.height * yMultiplier)))
      SetControlDelay -1
      ControlClick, %imgX% %imgY%, ahk_id %ANDROID_ID%,,,, NA
    }
    else {
      imgX := img.x + (img.width * xMultiplier) 
      imgY := img.y + (img.height * yMultiplier)
      MouseClick , left, imgX, imgY
    }
    sleep cooldown
    return true
  }
  else {
    return false
  }
}

findImage(fileName, imgX := 0, imgY := 0, imgWidth := 0, imgHeight := 0, useCache := false, activateEmulator := true) {
  ; if (activateEmulator)
  activateEmulator()
  needleFileName := "lib/img/" . fileName . ".png"
  didFind := false
  if (useCache && imgCache[fileName]) {
    return imgCache[fileName]
  }
  if (runInBackground) {
    ; debug("looking for " . needleFileName)
    If (!gdipToken := Gdip_Startup()) {
      error("gdiplus error!, Gdiplus failed to start. Please ensure you have gdiplus on your system")
      Reload
    }
    getCurrentScreen(imgHaystackFile)
    pHaystack := Gdip_CreateBitmapFromFile(imgHaystackFile)
    pNeedle := Gdip_CreateBitmapFromFile(needleFileName)
    gdipImg := Gdip_ImageSearch(pHaystack, pNeedle, gdipImgArr)
    Gdip_DisposeImage(pHaystack)
    Gdip_DisposeImage(pNeedle)
    Gdip_Shutdown(gdipToken)
    if (gdipImgArr.Length() > 0) {
      didFind := true
      imgX := gdipImgArr[1].x
      imgY := gdipImgArr[1].y
    }
  }
  else {
    ImageSearch, imgX, imgY, 0, 0, A_ScreenWidth, A_ScreenHeight, *TransBlack *50 %needleFileName%
    didFind := imgX > 0 || imgY > 0
  }
  if (didFind) {
    GetImageSize(needleFileName, imgWidth, imgHeight)
    r := { x: imgX, y: imgY, width: imgWidth, height: imgHeight}
    imgCache[fileName] := r
    return r
  }
  else {
    return false
  }
}

getCurrentScreen(imgFile) {
  Gdip_SaveBitmapToFile(getCurrentScreenBitmap(), imgFile)
}

getCurrentScreenBitmap() {
  return Gdip_BitmapFromHWND(getWindowId())
}

