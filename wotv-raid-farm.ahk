#NoEnv
#include lib/includes.ahk
#include mainScreen.ahk
SetBatchLines -1 ;-- To start

global timesRestarted := 0
global successfulGames := 0
global playersKicked := 0
global currentMethod := "none"
global switchRoomTime := 90000
global lastMethodChange := A_TickCount
global emergencyRestartLimit := 600000
global raidName := "2hd"
global raidSpecialName := "FlanPrincess"
global raidEnd := "raid" . raidName . "End"
global raidSpecialEnd := "raid" . raidSpecialName . "End"
global raidActive := "mainRaid" . raidName . "Active"
global raidClaim := "mainRaid" . raidName . "ClaimRewards"
global farplaneBannerFarm := "multiBannerAdmirablePrince"
global farplaneQuest := "multiQuestAdmirablePrince2"
global raidRoomSize := "raidRoom1"

; Gui New, -MaximizeBox
; Gui Add, Button, x40 y180 w50 h23, Start
; Gui Add, Button, x100 y180 w50 h23, Stop
; Gui Add, Button, x155 y180 w50 h23, Reset
; Gui Add, Button, x210 y180 w50 h23, Resume
; Gui Font, s14
; Gui Add, Text, x20 y6 w350 h23 +0x200 Center, WOTV
; Gui Font
; Gui Add, Text, x64 y36 w350 h23 +0x200, timesRestarted, %timesRestarted%
; Gui Add, Text, x64 y60 w350 h23 +0x200, successfulGames, %successfulGames%
; Gui Add, Text, x64 y84 w350 h23 +0x200, playersKicked, %playersKicked%
; Gui Add, Text, x64 y110 w350 h23 +0x200, currentMethod, %currentMethod%
; Gui Show, x1560 y145 w350 h210, WOTV Multi Farmer -- Host
whereAmI()
Return

OnError("restartEverything")

addSuccessfulGames() {
  successfulGames += 1
  ; GuiControl,, successfulGames, successfulGames %successfulGames%
}
addPlayersKicked() {
  playersKicked += 1
  ; GuiControl,, playersKicked, playersKicked %playersKicked%
}
addTimesRestarted() {
  timesRestarted += 1
  ; GuiControl,, timesRestarted, timesRestarted %timesRestarted%
}
updateCurrentMethod(method) {
  if (currentMethod == method) {
    debug("[RAID][updateCurrentMethod] still in the same method " . method . "; last change was at " . lastMethodChange)
    if (A_TickCount >= lastMethodChange + emergencyRestartLimit) {

      restartEverything()
    }

  }
  else {
    debug("[RAID][updateCurrentMethod] updating to " . method)
    lastMethodChange := A_TickCount
    currentMethod := method
    GuiControl,, currentMethod, currentMethod %currentMethod%
  }
}

start() {
  updateCurrentMethod("start")
  addTimesRestarted()
  notify("Starting RAID farmer!")
  debug("[RAID] starting main screen")
  accessGame()
  waitForImageAndClick("buttonNo", 50, 2000)
  goToMainScreen()
  accessRaid()
}

returnToTitle() {
  clickOnImage("buttonReturnToTitle")
  accessGame()
}

whereAmI() {
  updateCurrentMethod("whereAmI")
  debug("[RAID] trying to figure out where I am")
  attempts := 0
  activateEmulator()
  loop {
    if (attempts > 10) {
      debug( "I dunno where the fuck I am -- restarting")
      return restartEverything()
    }
    else if (findImage("gameStart"))
      return start()
    else if (findImage("raidResumeNo")){
      debug("i am in raidResumeNo")
      clickOnImage("raidResumeNo")
      Reload
    }
    else if (findImage("mainFarplane")){
      debug("i am in mainFarplane")
      return accessRaid()
    }
    else if(findImage("buttonReturnToTitle")){
      debug("i am in buttonReturnToTitle")
      return returnToTitle()
    }
    else if (findImage("raidQuickSearch")){
      debug("i am in raidQuickSearch")
      return createRoom()
    }
    else if(findImage("ingameAutoOn") || findImage("gameLoading") || findImage("ingameMenu")){
      debug("i am in buttonMenuIngame")
      return waitForGameEnd()
    }
    else if (findImage("raidNext") && (findImage(raidEnd) || findImage(raidSpecialEnd))){
      debug("i am in raidNext")
      return confirmResults()
    }
    else if (findImage("raidNext")){
      debug("i am in raidNext awaitForRaid()")
      return awaitForRaid()
    }
    else if (findImage("loadingScreen")){
      debug("loading screen, will wait for it to finish")
      sleep 1000
      Reload
    }
    else if(!isTheGameRunning()) {
      return restartEverything()
    }
    else {
      debug("still not clear, lets wait a little. attempt# " . attempts)
      clickOnImage("buttonMainMenu", 1000)
      clickOnImage("buttonHome") || clickOnImage("raidBack")
      attempts += 1
      sleep 2000
    }
  }
}

accessRaid() {
  updateCurrentMethod("accessRaid")
  debug("[RAID] accessing raid")
  if (!findImage("mainFarplane")) 
    Reload
  else if(findImage(raidActive)) 
    createRoom()
  else if(findImage(raidClaim)) 
    claimRewards()
  else
    accessEvent()
}

claimRewards() {
  updateCurrentMethod("claimRewards")
  debug("[RAID] claimRewards")
  waitForImageAndClick(raidClaim)
  waitForImageAndClick("raidButtonEnd")
  waitForImageAndClick("raidButtonClaimAll")
  waitForImageAndClick("raidClaimOk")
  Reload
}

accessEvent() {
  updateCurrentMethod("accessEvent")
  debug("[RAID] accessEvent")
  if(!clickOnImage("mainFarplane"))
    Reload
  waitForImageAndClick("farplaneEvents")
  i := 0
  while (!waitForImageAndClick(farplaneBannerFarm, 500, 2000) and i < 10) {
    waitForImageAndClick("buttonScrollDown", 500, 2000)
    i += 1
  }
  if (!waitForImageAndClick(farplaneQuest))
    return restartEverything()

  waitForImageAndClick("singlePlayerAutoRepeat")
  waitForImageAndClick("buttonSingleEmbark")
  awaitForRaid()
  return
}

awaitForRaid() {
  debug("will wait for raid to popup here")
  updateCurrentMethod("awaitForRaid")
  i := 1
  loop {
    if(waitForImageAndClick("raidChallengeNow"), 100) {
      debug("[awaitForRaid] found a raid! jumping in!")
      Reload
    }
    else if(findImage("ingameAutoOn") || findImage("gameLoading")) {
      debug("[awaitForRaid] still ingame, so waiting - " . i)
      i += 1
    }
    else {
      debug("[awaitForRaid] got lost again...")
      Reload
    }

  }
}

createRoom() {
  updateCurrentMethod("createRoom")
  clickOnImage(raidActive)
  debug("[RAID] checking orbs level")
  i := 0
  replenishOrbs()

  if (!findImage("raidChallenge")) {
    debug("[createRoom] no active raid, returning to main screen")
    clickOnImage("raidBack")
    Reload
  }
  else {
    waitForImageAndClick("raidChallenge", 50, 10000)
    waitForImageAndClick("raidChallenge2", 50, 10000)
    waitForImageAndClick(raidRoomSize, 50, 10000)
    waitForImageAndClick("raidCreateRoom", 1000, 10000)
    waitForImageAndClick("raidRoomEmbark", 2000, 10000)
    waitForImageAndClick("raidRoomEmbarkConfirm", 50, 2000)

    waitForGameEnd()
  }
}

replenishOrbs() {
  updateCurrentMethod("replenishOrbs")
  waitForImageAndClick("raidFillOrbs", 1000, 10000)
  if(!waitForImageAndClick("raidRestoreFailOk", 50, 2000)) {
    waitForImageAndClick("raidOrbPotion", 50, 5000) || waitForImageAndClick("raidOrbVis", 50, 5000)
    waitForImageAndClick("raidRestoreOrbs", 50, 5000)
    waitForImageAndClick("raidOk", 50, 5000)
  }
}

waitForGameEnd() {
  updateCurrentMethod("waitForGameEnd")
  ; confirm everything is running dandy
  debug("[waitForGameEnd] game running -- waiting for it to end")
  if(waitForImage("raidNext", 5000)){
    notify("Game finished!")
    confirmResults()
  }
  else if (findImage("ingameAutoOn") || findImage("ingameMenu")){
    debug("[waitForGameEnd] still ingame")
    Reload
  }
  else {
    debug("[waitForGameEnd] i'm lost!")
    Reload
  }
}

confirmResults() {
  updateCurrentMethod("confirmResults")

  waitForImageAndClick("raidNext", 2000, 10000)
  debug("[RAID] confirmResults - returning to room")
  waitForImageAndClick("raidCompleteQuest", 1000, 6000) || clickOnImage("raidTryAgainLater")
  addSuccessfulGames()

  waitForImageAndClick("raidClaimRewards", 2000, 5000)
  waitForImageAndClick("raidClaimSoloOk", 2000, 5000)
  waitForImageAndClick("raidBack", 2000, 10000)
  waitForImageAndClick("raidBack", 2000, 10000)
  notify("Game ended! Heading back to the room.")
  Reload
}

restartEverything() {
  notify("Something terrible happenned! Restarting everything!")
  updateCurrentMethod("restartEverything")
  killEmulator()
  addTimesRestarted()
  accessGame()
  waitForImageAndClick("raidResumeNo", 500, 5000)
  start()
}

ButtonStart:
  start()

ButtonReset:
  Reload

ButtonStop:
Escape:
GuiClose:
ExitApp

ButtonResume:
  whereAmI()
