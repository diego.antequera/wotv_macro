#NoEnv
#include lib/includes.ahk
#include mainScreen.ahk
SetBatchLines -1 ;-- To start

global timesRestarted := 0
global successfulGames := 0
global playersKicked := 0
global currentMethod := "none"
global switchRoomTime := 90000
global lastMethodChange := A_TickCount
global emergencyRestartLimit := 600000

Gui New, -MaximizeBox
Gui Add, Button, x40 y180 w50 h23, Start
Gui Add, Button, x100 y180 w50 h23, Stop
Gui Add, Button, x155 y180 w50 h23, Reset
Gui Add, Button, x210 y180 w50 h23, Resume
Gui Font, s14
Gui Add, Text, x20 y6 w350 h23 +0x200 Center, WOTV
Gui Font
Gui Add, Text, x64 y36 w350 h23 +0x200, timesRestarted, %timesRestarted%
Gui Add, Text, x64 y60 w350 h23 +0x200, successfulGames, %successfulGames%
Gui Add, Text, x64 y84 w350 h23 +0x200, playersKicked, %playersKicked%
Gui Add, Text, x64 y110 w350 h23 +0x200, currentMethod, %currentMethod%
Gui Show, x1560 y145 w350 h210, WOTV Multi Farmer -- Host
whereAmI()
Return

OnError("restartEverything")

addSuccessfulGames() {
  successfulGames += 1
  ; GuiControl,, successfulGames, successfulGames %successfulGames%
}
addPlayersKicked() {
  playersKicked += 1
  ; GuiControl,, playersKicked, playersKicked %playersKicked%
}
addTimesRestarted() {
  timesRestarted += 1
  ; GuiControl,, timesRestarted, timesRestarted %timesRestarted%
}
updateCurrentMethod(method) {
  if (currentMethod == method) {
    debug("[RAID][updateCurrentMethod] still in the same method " . method . "; last change was at " . lastMethodChange)
    if (A_TickCount >= lastMethodChange + emergencyRestartLimit) {

      restartEverything()
    }

  }
  else {
    debug("[RAID][updateCurrentMethod] updating to " . method)
    lastMethodChange := A_TickCount
    currentMethod := method
    GuiControl,, currentMethod, currentMethod %currentMethod%
  }
}

start() {
  updateCurrentMethod("start")
  addTimesRestarted()
  notify("Starting RAID farmer!")
  debug("[RAID] starting main screen")
  accessGame()
  goToMainScreen()
  accessRaid()
}

returnToTitle() {
  clickOnImage("buttonReturnToTitle")
  accessGame()
}

whereAmI() {
  updateCurrentMethod("whereAmI")
  debug("[RAID] trying to figure out where I am")
  attempts := 0
  activateEmulator()
  loop {
    if (attempts > 5) {
      debug( "I dunno where the fuck I am -- restarting")
      restartEverything()
    }
    else if (findImage("mainFarplane")){
      debug("i am in mainFarplane")
      accessRaid()
    }
    else if(findImage("buttonReturnToTitle")){
      debug("i am in buttonReturnToTitle")
      returnToTitle()
    }
    else if (findImage("raidQuickSearch")){
      debug("i am in raidQuickSearch")
      findRoom()
    }
    else if (findImage("raidCancelPreparation") || findImage("raidKickedOk") || findImage("raidKickedOk2")){
      debug("i am in raidCancelPreparation")
      waitForGameStart()
    }
    else if(findImage("buttonMenuIngame") || findImage("gameLoading")){
      debug("i am in buttonMenuIngame")
      waitForGameEnd()
    }
    else if (findImage("raidReady")){
      debug("i am in raidReady")
      getReady()
    }
    else if (findImage("raidNext")){
      debug("i am in raidNext")
      confirmResults()
    }
    else if (findImage("loadingScreen")){
      debug("loading screen, will wait for it to finish")
      sleep 1000
      Reload
    }
    else {
      debug("still not clear, lets wait a little. attempt#" . attemps)
      attempts += 1
      sleep 2000
    }
  }
}

accessRaid() {
  updateCurrentMethod("accessRaid")
  debug("[RAID] accessing raid")
  if(!waitForImageAndClick("mainRaidBehemoth"))
    restartEverything()
  findRoom()
}

findRoom() {
  updateCurrentMethod("findRoom")
  debug("[RAID] checking orbs level")
  i := 0
  if (findImage("raidOrbsEmpty")) {
    return replenishOrbs()
  }
  waitForImageAndClick("raidQuickSearch", 50, 10000)
  ; waitForImageAndClick("raidSearchLevel", 50, 10000)
  ; i := 0
  ; while(!clickOnImage("raidLevel91") || i < 5) {
  ;   clickOnImage("raidSearchLevelBar")
  ;   i++
  ; }

  waitForImageAndClick("raidSearch", 50, 10000)
  getReady()
}

replenishOrbs() {
  if (waitForImage("raidOrbsEmpty"), 5000, 50) {
    clickOnImage("raidFillOrbs")
    waitForImageAndClick("raidOrbPotion", 50, 5000) || waitForImageAndClick("raidOrbVis", 50, 5000)
    waitForImageAndClick("buttonYesGeneric")
    waitForImageAndClick("raidOk")
  }
  Reload
}

getReady() {
  if(!waitForImageAndClick("raidReady", 50, 10000)) {
    Reload
  }
  else {
    waitForGameStart()
  }

}

waitForGameStart() {
  _starttime := A_TickCount 
  updateCurrentMethod("waitForGameStart")
  debug("[RAID] waitForGameStart")
  loop {
    elapsedtime := A_TickCount - _starttime
    debug("[RAID] waiting for " . elapsedtime)
    if (findImage("raidKickedOk") || findImage("raidKickedOk2")) {
      debug("got kicked!")
      sleep 2000
      clickOnImage("raidKickedOk")
      clickOnImage("raidKickedOk2")
      findRoom()
      return 
    }
    else if (findImage("raidCancelPreparation") && elapsedtime > switchRoomTime) {
      debug("tired of waiting, moving on")
      exitRoom()
      break
    }
    else if (findImage("raidCancelPreparation")) {
      sleep 500
      continue
    }
    else if (findImage("buttonMenuIngame")) {
      waitForGameEnd()
      break
    }
    else if (findImage("gameLoading")) {
      debug("[waitForGameStart] game is loading")
      waitForGameStart()
      break
    }
    else {
      debug("[waitForGameStart] got lost again...")
      Reload
      break
    }
  }
}

exitRoom() {
  updateCurrentMethod("exitRoom")
  debug("[RAID] exitRoom")
  waitForImageAndClick("raidCancelPreparation", 1000, 10000)
  waitForImageAndClick("raidLeaveRoom", 1000, 10000)
  sleep 2000
  whereAmI()
}

waitForGameEnd() {
  updateCurrentMethod("waitForGameEnd")
  ; confirm everything is running dandy
  debug("[waitForGameEnd] game running -- waiting for it to end")
  if(waitForImage("raidNext", 5000)){
    notify("Game finished!")
    confirmResults()
  }
  else if (findImage("buttonMenuIngame") || waitForImage("buttonMenuIngame", 5000)){
    debug("[waitForGameEnd] still ingame")
    Reload
  }
  else if(waitForImage("raidCancelPreparation", 2000)){
    debug("[waitForGameEnd] oops, looks like still in the waiting room")
    waitForGameStart()
  }
  else {
    debug("[waitForGameEnd] i'm lost!")
    Reload
  }
}

confirmResults() {
  updateCurrentMethod("confirmResults")
  debug("[RAID] confirmResults - denying friend requests")

  clickOnImage("raidNext")
  waitForImageAndClick("raidRewardOk", 750, 5000)
  waitForImageAndClick("buttonCancel", 750, 5000)
  debug("[RAID] confirmResults - returning to room")
  sleep 5000
  if(clickOnImage("raidCompleteQuest") || clickOnImage("raidLeaveBattle"))
    addSuccessfulGames()

  waitForImageAndClick("raidBack", 2000, 10000, 2000)
  notify("Game ended! Heading back to the room.")
  Reload
}

restartEverything() {
  notify("Something terrible happenned! Restarting everything!")
  updateCurrentMethod("restartEverything")
  killEmulator()
  addTimesRestarted()
  accessGame()
  start()
}

ButtonStart:
  start()

ButtonReset:
  Reload

ButtonStop:
Escape:
GuiClose:
  ExitApp

ButtonResume:
  whereAmI()
