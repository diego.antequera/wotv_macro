﻿#NoEnv
#include lib/includes.ahk
#include mainScreen.ahk
SetBatchLines -1 ;-- To start

global timesRestarted := 0
global successfulGames := 0
global playersKicked := 0
global currentMethod := "none"
global doSolo := true
global doSoloWaitTime := 1000
global craftItem := "equipmentGoldenBlade"
global craftItemType := "Assault"
global totalCraftQuantity := 32
global craftedAmount := 0
global lastMethodChange := A_TickCount
global emergencyRestartLimit := 600000

Gui New, -MaximizeBox
Gui Add, Button, x40 y180 w50 h23, Start
Gui Add, Button, x100 y180 w50 h23, Stop
Gui Add, Button, x155 y180 w50 h23, Reset
Gui Add, Button, x210 y180 w50 h23, Resume
Gui Font, s14
Gui Add, Text, x20 y6 w350 h23 +0x200 Center, WOTV
Gui Font
Gui Add, Text, x64 y36 w350 h23 +0x200, timesRestarted, %timesRestarted%
Gui Add, Text, x64 y60 w350 h23 +0x200, successfulGames, %successfulGames%
Gui Add, Text, x64 y84 w350 h23 +0x200, playersKicked, %playersKicked%
Gui Add, Text, x64 y110 w350 h23 +0x200, currentMethod, %currentMethod%
Gui Show, x1560 y145 w350 h210, WOTV Multi Farmer -- Host
plusTest := 4
enhanceItem(plusTest)
; craftPlus(plusTest)
Return

OnError("restartEverything")

addSuccessfulGames() {
  successfulGames += 1
  GuiControl,, successfulGames, successfulGames %successfulGames%
}
addPlayersKicked() {
  playersKicked += 1
  GuiControl,, playersKicked, playersKicked %playersKicked%
}
addTimesRestarted() {
  timesRestarted += 1
  GuiControl,, timesRestarted, timesRestarted %timesRestarted%
}
updateCurrentMethod(method) {
  if (currentMethod == method) {
    debug(" [CRAFT][updateCurrentMethod] still in the same method " . method . "; last change was at " . lastMethodChange)
    if (A_TickCount >= lastMethodChange + emergencyRestartLimit) {

      start()
    }

  }
  else {
    debug(" [CRAFT][updateCurrentMethod] updating to " . method)
    lastMethodChange := A_TickCount
    currentMethod := method
    GuiControl,, currentMethod, currentMethod %currentMethod%
  }
}

start() {
  updateCurrentMethod("start")
  addTimesRestarted()
  notify("Starting CRAFT farmer!")
  debug("[CRAFT] starting main screen")
  craftedAmount := 0
  accessGame()
  goToMainScreen()
  accessCraft()
}

returnToTitle() {
  clickOnImage("buttonReturnToTitle")
  accessGame()
}

accessCraft() {
  updateCurrentMethod("accessCraft")
  debug(" [CRAFT] accessing craft")
  if(!waitForImageAndClick("mainEquipment"))
    start()
  waitForImageAndClick("equipmentCrafting")
  craft()
}

craft() {
  debug("[CRAFT] crafting" . craftItem)
  craftedAmount := 0
  while (craftedAmount < totalCraftQuantity) {
    updateCurrentMethod("crafting " . craftItem . " - " . craftedAmount . "/" . totalCraftQuantity)
    debug("[CRAFT] crafting " . craftItem . " - " . craftedAmount . "/" . totalCraftQuantity)

    i := 0
    waitForImage("equipmentScroll")
    while (!clickOnImage(craftItem) and i < 10) {
      waitForImageAndClick("equipmentScroll")
      i += 1
    }
    debug(11)

    waitForImageAndClick("buttonCraft")
    waitForImageAndClick("buttonYes")
    waitForImageAndClick("equipmentCraftingComplete")
    craftedAmount += 1
  }

  craftPlus(1)
}

craftPlus(plus) {
  craftItemPlus := craftItem . "Plus"
  craftItemSelect := craftItem . "Select"
  craftItemTypeName := "equipmentButtonType" . craftItemType
  debug("[CRAFT] crafting " . craftItem)

  craftedAmount := 0
  craftQuantity := totalCraftQuantity / (2 ** plus)
  debug("[CRAFT] craftedAmount " . craftQuantity)
  debug("[CRAFT] craftQuantity " . craftedAmount)
  debug("[CRAFT] plus " . (2 ** plus))
  while (craftedAmount < craftQuantity) {
    updateCurrentMethod("crafting " . craftItemPlus . " + " . plus . " - " . craftedAmount . "/" . craftQuantity)
    debug("[CRAFT] crafting " . craftItemPlus . " + " . plus . " - " . craftedAmount . "/" . craftQuantity)

    i := 0
    waitForImage("equipmentScroll")
    while (!clickOnImage(craftItemPlus) and i < 10) {
      waitForImageAndClick("equipmentScroll")
      i += 1
    }

    waitForImageAndClick("buttonCraft")

    if (waitForImage("equipmentCraftSelectMaterials", 2000)) {
      waitForImageAndClick(craftItemSelect)
      waitForImageAndClick(craftItemSelect)
      waitForImageAndClick("buttonConfirm")
    }

    if (waitForImage("equipmentCraftInheritType", 1000) && !waitForImageAndClick(craftItemTypeName, 50, 1000)) {
      clickOnImage("equipmentButtonTypeAny")
      waitForImageAndClick("buttonYesGeneric")
    }

    waitForImageAndClick("buttonYesSmall")
    waitForImageAndClick("equipmentCraftingComplete")
    craftedAmount += 1
    waitForImageAndClick("buttonOk", 50, 10000)
  }

  ; return to equipment menu
  clickOnImage("buttonBack")
  enhanceItem(plus)
}

enhanceItem(plus) {
  enhanceItem := craftItem . "Enhance"
  enhancededAmount := 0
  enhanceQuantity := totalCraftQuantity / (plus ** plus)
  debug("[CRAFT] enhancing " . craftItem)
  if (!findImage("equipmentEnhance")) {
    ; enter first available item
    waitForImageAndClick("equipmentEnhanceFirstItem", 50, 5000, 2000, 50)
  }
  while (enhancededAmount < enhanceQuantity) {
    updateCurrentMethod("enhancing " . enhanceItem . " + " . plus . " - " . enhancededAmount . "/" . enhanceQuantity)
    debug("[CRAFT] enhancing " . enhanceItem . " + " . plus . " - " . enhancededAmount . "/" . enhanceQuantity)

    ; confirm if in correct item
    while (!(findImage("equipmentGoldBladeEnhancePageItem") && findImage("equipmentEnhanceLevel1"))) {
      waitForImageAndClick("equipmentEnhanceNext")
    }

    awakened := 0
    while (awakened < plus) {
      debug("[equipmentEnhance]")
      waitForImageAndClick("equipmentEnhance", 50, 20000)
      debug("[buttonMax]")
      waitForImageAndClick("buttonMax", 50, 2000)
      debug("[buttonEnhance]")
      waitForImageAndClick("buttonEnhance", 50, 2000)
      debug("[buttonBeginEnhancement]")
      waitForImageAndClick("buttonBeginEnhancement", 50, 2000)
      debug("[buttonYes]")
      waitForImageAndClick("buttonYes", 50, 2000)
      debug("[buttonYesSmall]")
      waitForImageAndClick("buttonYesSmall", 50, 20000, 2000)

      debug("[if]")
      if (awakened < plus) {
        debug("[equipmentEnhanceAwaken]")
        waitForImageAndClick("equipmentEnhanceAwaken", 50, 20000)
        debug("[buttonAwaken]")
        waitForImageAndClick("buttonAwaken", 50, 2000)
        debug("[buttonYes]")
        waitForImageAndClick("buttonYesSmall", 50, 2000, 2000)
        debug("[equipmentAwakenEnhance]")
        waitForImageAndClick("equipmentAwakenEnhance", 50, 20000, 2000)
      }
      debug("[endif]")
      awakened += 1
    }

    if (plus > 1) {
      waitForImageAndClick("buttonYesSmall", 50, 2000)
    }

    enhancededAmount += 1
    i := 0
    while(!findImage("equipmentEnhance") and i < 10) {
      waitForImageAndClick("buttonBack")
      i += 1
    }
  }

  i := 0
  while(!findImage("equipmentCrafting") and i < 10) {
    waitForImageAndClick("buttonBack")
    i += 1
  }
  waitForImageAndClick("equipmentCrafting")
  craftPlus(plus + 1)
}

ButtonStart:
  start()

ButtonReset:
  Reload

ButtonStop:
Escape:
GuiClose:
  ExitApp

ButtonResume:
  ; whereAmI()
