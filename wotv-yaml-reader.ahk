﻿#NoEnv
#include lib/includes.ahk
#include mainScreen.ahk
SetBatchLines -1 ;-- To start

; global yamlFile := "event-tower.yaml"
global yamlFile := "multi-raid.yaml"
global runInfoFile := "_runInfo.yaml"
global currentMethod := "none"
global lastMethodChange := A_TickCount
global emergencyRestartLimit := 600000
global yaml := false

; STEP CONTROL VARS
global steps
global totalSteps := 0
global currentStepIndex := 0
global currentRetries := 0
global awaitingSince := false
global previousSuccessStepIndex := -1
global lastSuccessTimestamp := A_TickCount

; YAML CONFIG VARS
global autoName := "Undefined"
global delayBetweenSteps := 2000
global imageCache := true
global imageSearchSleepTime := 500
global maxRetries := 10
global isSaving := false

whereAmI()
Return

; yaml template
; steps:
;   #image name that will be clicked (should be unique)
;   - name: 
;     # resets everything if succesful
;     reset: true
;     #only actually clicks if the image name in condition is found
;     condition: 
;     #if should click in an image diferent than the name
;     clickOn:
;     #waits for the specified time
;     waitFor:
;     #waits to go to the next step until this image is found or the time is up (which will cause a fail)
;     waitUntil: 
;       image: 
;       time:
;     #list of alternate images to look for to confirm that it is on this step
;     identifiers: 
;     #list of images to click in case it fails
;     fallback: 
;       #image name to try to click
;       - image: 
;         #if true, follow to the next fallback
;         continue: 
;         #go to this step
;         goTo: 
;     
loadYaml() {
  if (!yaml) {
    yaml := Yaml(yamlFile)
    steps := yaml.steps
    totalSteps := steps.()

    runInfo := Yaml(runInfoFile)
    currentStepIndex := runInfo.currentStepIndex
    currentRetries := runInfo.currentRetries
    awaitingSince := runInfo.awaitingSince
    previousSuccessStepIndex := runInfo.previousSuccessStepIndex
    lastSuccessTimestamp := runInfo.lastSuccessTimestamp
    LAST_SLEEP_PREVENT := runInfo.LAST_SLEEP_PREVENT
    parseConfig()
  }
}

parseConfig() {
  if (yaml.config.name)
    autoName := yaml.config.name
  if (yaml.config.delayBetweenSteps)
    delayBetweenSteps := yaml.config.delayBetweenSteps
  if (yaml.config.useImageCache)
    imageCache := yaml.config.useImageCache
  if (yaml.config.imageSearchSleepTime)
    imageSearchSleepTime := yaml.config.imageSearchSleepTime
  if (yaml.config.maxRetries)
    maxRetries := yaml.config.maxRetries
  if (yaml.config.emergencyRestartLimit)
    emergencyRestartLimit := yaml.config.emergencyRestartLimit
}

refreshFile() {
  obj := Yaml("currentStepIndex: " . currentStepIndex . "`npreviousSuccessStepIndex: " . previousSuccessStepIndex . "`nlastSuccessTimestamp: " . lastSuccessTimestamp . "`ncurrentRetries: " . currentRetries . "`nawaitingSince: " . awaitingSince . "`nLAST_SLEEP_PREVENT: " . LAST_SLEEP_PREVENT, 0)
  if (isSaving) {
    sleep 350
    refreshFile()
  }
  else {
    isSaving := true
    Yaml_Save(obj, runInfoFile)
    isSaving := false
  }
}

updateCurrentMethod(method) {
  currentMethod := method
}

addStep() {
  setCurrentStep(currentStepIndex + 1)
}

setCurrentStep(i) {
  currentStepIndex := i
  refreshFile()
}

addRetry() {
  currentRetries += 1
  refreshFile()
  if (currentRetries > maxRetries) {
    log("restarting everything")
    restartEverything()
  }
}

whereAmI() {
  loadYaml()

  updateCurrentMethod("whereAmI")
  log("trying to find where I am")
  match := false
  if (!currentStepIndex) {
    setCurrentStep(1)
  }
  loop {
    if (currentStepIndex > totalSteps) {
      log("still not clear, will try again (attempt " . currentRetries . "/" . maxRetries . ")")
      setCurrentStep(1)
      sleep 2000
      runFallbacks(yaml.fallback)
    }
    currentStep := steps.(currentStepIndex)
    log("testing step " . currentStepIndex . " - " . currentStep.name)

    match := isInStep(currentStep, (match ? true : false))
    if (match) {
      log("moving to step " . currentStepIndex . " - " . currentStep.name)
      ; if in the same step for long, reset
      if (currentStepIndex == previousSuccessStepIndex && A_TickCount >= lastSuccessTimestamp + emergencyRestartLimit) {
        log("stuck on step " . currentStepIndex . " for too long! restarting")
        restartEverything()
      }

      runStep(currentStep)
      return 
    }

    addStep()
  }
  restartEverything()
}

isInStep(step, doDelay := true) {
  imageName := step.name
  delay := coalesce(step.waitFor, delayBetweenSteps)
  image := (doDelay ? waitForImage(imageName, delay) : findImage(imageName))
  if (image) 
    return image
  if (step.identifiers) {
    for i, imageName in step.identifiers[""] {
      image := findImage(imageName)
      if (image) {
        return image
      }
    }
  }
  return image
}

runStep(step) {
  updateCurrentMethod(step.name)
  imageName := coalesce(step.clickOn, step.name)

  if (step.delay) {
    sleep step.delay
  }

  if(step.action == "await") {
    log("On the right place, just gotta wait.")
    if (!awaitingSince) {
      log("no awaitingSince, so waiting up")
      awaitingSince := A_TickCount
    }
    else if(A_TickCount >= awaitingSince + step.timeup) {
      return restartEverything()
    }
    sleep 500
    currentRetries := 0
    refreshFile()
    Reload
    return
  }

  stepSuccess := clickOnImage(imageName, cooldown := 200, xMultiplier := 0.5, yMultiplier := 0.5, useCache := imageCache)
  if (!stepSuccess && step.search) {
    stepSuccess := searchWhileNotClick(imageName, step.search)
  }

  if (stepSuccess && step.waitUntil) {
    stepSuccess := waitForImage(step.waitUntil.image, step.waitUntil.time)
  }

  if (stepSuccess || step.optional) {
    awaitingSince := false
    currentRetries := 0
    if (previousSuccessStepIndex != currentStepIndex) {
      previousSuccessStepIndex := currentStepIndex
      lastSuccessTimestamp := A_TickCount
    }

    nextStep(step)
  }
  else if (step.fallback) {
    runFallbacks(step.fallback)
  }
  else {
    addRetry()
    Reload
  }
}

nextStep(step) {
  sleep 500
  if (step.reset) {
    Reload
  }
  else if (step.goTo) {
    goToStep(step.goTo)
  }
  else {
    addStep()
    if (currentStepIndex > totalSteps) {
      setCurrentStep(1)
    }
    currentStep := steps.(currentStepIndex)
    tryStep(currentStep)
  }
}

goToStep(stepName) {
  updateCurrentMethod("goToStep")
  log("going to step " . stepName)
  for i, step in steps[""] {
    if (step.name == stepName) {
      setCurrentStep(i)
      return tryStep(step)
      break
    }
  }
  log("step doesnt exist, reloading")
  Reload
}

tryStep(step) {
  updateCurrentMethod("tryStep")
  log("checking step " . step.name)
  if (isInStep(step, delayBetweenSteps)) {
    runStep(step)
  }
  else {
    whereAmI()
  }
}

runFallbacks(fallbacks) {
  addRetry()
  updateCurrentMethod("fallback")
  for i, fallback in fallbacks[""] {
    log("fallback " . fallback.image)
    success := clickOnImage(fallback.image)
    if (success && fallback.delay) {
      sleep fallback.delay
    }
    if (success && fallback.goTo) {
      return goToStep(fallback.goTo)
    }
    if (success && !fallback.continue) {
      break
    }
  }
  ; if it gets here, it means that either no fallbacks were successful, or none of them had goTo
  setCurrentStep(1)
  sleep 2000

  Reload
}

searchWhileNotClick(imageToClick, imageToSearch, maxSearch := 10){
  isSuccessful := false
  i := 0
  while (!(isSuccessful := clickOnImage(imageToClick)) && i < maxSearch) {
    waitForImageAndClick(imageToSearch, 500, 2000)
    i += 1
  }
  return isSuccessful
}

log(msg) {
  debug("[" . autoName . "] " . "[" . currentMethod . "] " . msg)
}

restartEverything() {
  currentRetries := 0
  awaitingSince := false
  previousSuccessStepIndex := -1
  lastSuccessTimestamp := A_TickCount
  refreshFile()
  notify("Something terrible happenned! Restarting everything!")
  updateCurrentMethod("restartEverything")
  killEmulator()
  accessGame()
  whereAmI()
}

coalesce(value1, value2) {
  if (value1)
    return value1
  return value2
}
Escape:
GuiClose:
ExitApp
