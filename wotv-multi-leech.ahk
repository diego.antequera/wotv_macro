﻿#NoEnv
#include lib/includes.ahk
#include mainScreen.ahk
SetBatchLines -1 ;-- To start

global timesRestarted := 0
global successfulGames := 0
global playersKicked := 0
global currentMethod := "none"
global switchRoomTime := 50000
global multiBanner := "multiBannerShiva"
global multiQuest := "multiQuestShiva"
global lastMethodChange := A_TickCount
global emergencyRestartLimit := 600000

if (displayController) {
  Gui New, -MaximizeBox
  Gui Add, Button, x40 y180 w50 h23, Start
  Gui Add, Button, x100 y180 w50 h23, Stop
  Gui Add, Button, x155 y180 w50 h23, Reset
  Gui Add, Button, x210 y180 w50 h23, Resume
  Gui Font, s14
  Gui Add, Text, x20 y6 w350 h23 +0x200 Center, WOTV
  Gui Font
  Gui Add, Text, x64 y36 w350 h23 +0x200, timesRestarted, %timesRestarted%
  Gui Add, Text, x64 y60 w350 h23 +0x200, successfulGames, %successfulGames%
  Gui Add, Text, x64 y84 w350 h23 +0x200, playersKicked, %playersKicked%
  Gui Add, Text, x64 y110 w350 h23 +0x200, currentMethod, %currentMethod%
  Gui Show, x1560 y145 w350 h210, WOTV Multi Farmer -- Host
}
whereAmI()
Return

OnError("restartEverything")

addSuccessfulGames() {
  successfulGames += 1
  GuiControl,, successfulGames, successfulGames %successfulGames%
}
addPlayersKicked() {
  playersKicked += 1
  GuiControl,, playersKicked, playersKicked %playersKicked%
}
addTimesRestarted() {
  timesRestarted += 1
  GuiControl,, timesRestarted, timesRestarted %timesRestarted%
}
updateCurrentMethod(method) {
  if (currentMethod == method) {
    debug("[MULTI][updateCurrentMethod] still in the same method " . method . "; last change was at " . lastMethodChange)
    if (A_TickCount >= lastMethodChange + emergencyRestartLimit) {

      restartEverything()
    }

  }
  else {
    debug("[MULTI][updateCurrentMethod] updating to " . method)
    lastMethodChange := A_TickCount
    currentMethod := method
    GuiControl,, currentMethod, currentMethod %currentMethod%
  }
}

start() {
  updateCurrentMethod("start")
  addTimesRestarted()
  notify("Starting MULTI farmer!")
  debug("[MULTI] starting main screen")
  accessGame()
  goToMainScreen()
  accessMulti()
}

returnToTitle() {
  clickOnImage("buttonReturnToTitle")
  accessGame()
}

whereAmI() {
  updateCurrentMethod("whereAmI")
  debug("[MULTI] trying to figure out where I am")
  attempts := 0
  activateEmulator()
  loop {
    if (attempts > 7) {
      debug( "I dunno where the fuck I am -- restarting")
      restartEverything()
    }
    else if (findImage("labelFailedToReadFile")) {
      clickOnImage("buttonGenericConfirm")
      continue
    }
    else if (clickOnImage("multiRejoinRoomNo")) {
      debug("im at multiRejoinRoomNo")
      continue
    }
    else if (findImage("mainFarplane")) {
      debug("im at accessMulti")
      accessMulti()
    }

    else if(findImage("buttonReturnToTitle")) {
      debug("im at returnToTitle")
      returnToTitle()
    }

    else if (findImage("multiCancelPreparation") || findImage("multiRoomClosed")) {
      waitForGameStart()
    }

    else if (findImage("multiLabel")) {
      debug("im at findRoom")
      findRoom()
    }

    else if (findImage("multiRoomReady")) {
      debug("im at multiRoomReady")
      getReady()
    }

    else if(findImage("ingameMenu")) {
      debug("im at waitForGameEnd")
      waitForGameEnd()
    }

    else if (findImage("buttonNext")) {
      debug("im at confirmResults")
      confirmResults()
    }

    else if (findImage("buttonMainMenu")) {
      debug("im at start")
      start()
    }

    else if (findImage("gameStart")) {
      debug("im at start")
      start()
    }

    else if(findImage("multiRecruitClose") || clickOnImage("multiJoinRoom")) {
      enterRoom()
    }

    else if(findImage(multiQuest) || findImage("multiLabel")) {
      findRoom()
    }

    else if(clickOnImage("buttonGenericConfirm")) {
      debug("Generic confirm worked! Trying to figure out again")
      continue
    }
    else {
      debug("still not clear, lets wait a little. attempt#" . attempts)
      clickOnImage("buttonMainMenu", 1000) || clickOnImage("buttonGenericConfirm")
      clickOnImage("buttonHome") || clickOnImage("raidBack")
      attempts += 1
      sleep 2000
    }
  }
}

accessMulti() {
  updateCurrentMethod("accessMulti")
  debug("[MULTI] accessing multi")
  if(!waitForImageAndClick("mainFarplane"))
    restartEverything()
  waitForImageAndClick("farplaneMulti")
  findRoom()
}

findRoom() {
  updateCurrentMethod("findRoom")
  debug("[MULTI] finding multi room")
  i := 0
  if (!waitForImage("multiLabel", 10000)) {
    Reload
  }
  while (!clickOnImage(multiBanner) and i < 10) {
    waitForImageAndClick("buttonScrollDown")
    i += 1
  }

  if(!waitForImageAndClick(multiQuest, 500, 5000))
    Reload

  waitForImageAndClick("multiJoinRoom", 500, 4000)
  enterRoom()
}

enterRoom() {
  updateCurrentMethod("enterRoom")
  debug("[MULTI] entering multi room")
  i := 0
  loop {
    clickOnImage("multiRejoinRoomNo")
    clickOnImage("multiAutoOkRoom")
    if(waitForImage("multiRoomReady", 4000)) {
      getReady()
      break
    }
    else if (i > 10) {
      Reload
      break
    }
    else if(clickOnImage("buttonGenericConfirm") || clickOnImage("multiListUpdate")) {
      continue
    }
    else {
      Reload
      break
    }
  }
}

getReady() {
  updateCurrentMethod("getReady")
  if(!waitForImageAndClick("multiRoomReady", 50, 10000)) {
    Reload
  }
  else {
    waitForGameStart()
  }
}

waitForGameStart() {
  _starttime := A_TickCount 
  updateCurrentMethod("waitForGameStart")
  debug("[multi] waitForGameStart")
  loop {
    elapsedtime := A_TickCount - _starttime
    debug("[multi] waiting for " . elapsedtime . "/" . switchRoomTime)
    if (findImage("raidKickedOk") || findImage("multiRoomClosed")) {
      debug("got kicked!/room closed")
      sleep 2000
      clickOnImage("raidKickedOk")
      clickOnImage("multiRoomClosed")
      Reload
    }
    else if (findImage("multiCancelPreparation") && elapsedtime > switchRoomTime) {
      debug("tired of waiting, moving on")
      exitRoom()
      break
    }
    else if (waitForImage("multiCancelPreparation", 4000)) {
      sleep 500
      continue
    }
    else if (findImage("buttonMenuIngame")) {
      waitForGameEnd()
      break
    }
    else if (findImage("gameLoading")) {
      debug("[waitForGameStart] game is loading")
      waitForGameStart()
      break
    }
    else {
      debug("[waitForGameStart] got lost again...")
      Reload
      break
    }
  }
}

exitRoom() {
  updateCurrentMethod("exitRoom")
  debug("[MULTI] exitRoom")
  waitForImageAndClick("multiCancelPreparation", 1000, 10000)
  waitForImageAndClick("multiLeaveRoom", 1000, 10000)

  sleep 2000
  whereAmI()
}

waitForGameEnd() {
  updateCurrentMethod("waitForGameEnd")
  ; confirm everything is running dandy
  debug("[waitForGameEnd] game running -- waiting for it to end")
  if(waitForImage("raidNext", 5000)){
    notify("Game finished!")
    confirmResults()
  }
  else if (findImage("ingameMenu") || waitForImage("ingameMenu", 5000)){
    debug("[waitForGameEnd] still ingame")
    Reload
  }
  else if(waitForImage("multiCancelPreparation", 2000)){
    debug("[waitForGameEnd] oops, looks like still in the waiting room")
    waitForGameStart()
  }
  else {
    debug("[waitForGameEnd] i'm lost!")
    Reload
  }
}

confirmResults() {
  updateCurrentMethod("confirmResults")
  debug("[MULTI] confirmResults - denying friend requests")

  sleep 1000
  clickOnImage("buttonNext")
  waitForImageAndClick("buttonCancel", 1000, 5000)
  debug("[MULTI] confirmResults - returning to room")
  waitForImageAndClick("buttonReturn", 1000, 10000)
  Reload
}

restartEverything() {
  notify("Something terrible happenned! Restarting everything!")
  updateCurrentMethod("restartEverything")
  killEmulator()
  addTimesRestarted()
  accessGame()
  whereAmI()
}

ButtonStart:
  start()

ButtonReset:
  Reload

ButtonStop:
Escape:
GuiClose:
ExitApp

ButtonResume:
  whereAmI()
